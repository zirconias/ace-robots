package com.imdrissi.rbc.acerobots;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AceRobotsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AceRobotsApplication.class, args);
	}
}
